# Chat grupal utilizando Sockets y el protocolo TCP/IP en Java

## Tabla de contenidos

* [Miembros del equipo](#miembros)
* [Cosas necesarias para la compilación del proyecto](#materiales)
* [Modo de uso](#uso-cliente)





## <a name="miembros"></a>Miembros del equipo
* "Alen Figueroa" <alenfigueroam@lgmail.com>
* "Marcelo Becerra" <imaberro90@gmail.com>
* "Javier Peña" <javpenareyes@gmail.com>

## <a name="materiales"></a>Cosas necesarias para la compilación del proyecto

* Netbeans.
* JavaFx instalado.
* En caso de no querer modificar el puerto, tener el 5005 habilitado.



## <a name="uso-cliente"></a>Modo de uso

* Abra el proyecto en netbeans


* Para iniciar el servidor, haga click derecho en el archivo "ChatServer.java" y seleccionar "Run file"

![Picture](img/1.png)


* Para abrir una ventana de chat, haga click derecho en el archivo "MainCliente.java" y seleccione "Run file"

![Picture](img/2.png)


* Dentro de la ventana que se abre, coloque su nombre de usuario y envíe para iniciar el chat.

![Picture](img/3.png)

![Picture](img/4.png)





