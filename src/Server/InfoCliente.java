package Server;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
/**
 * Almacena la información relevante de un cliente determinado
 */
public class InfoCliente{

    private Socket socket;
    private String nombreUsuario;

    public InfoCliente(String usuario,Socket sock){
        nombreUsuario = usuario;
        socket = sock;
    }


/*--------------------Getters---------------------------------- */
    public String getNombreUsuario(){
        return nombreUsuario;
    }
    
    public Socket getSocket(){
        return socket;
    }
    
    public InputStream getInputStream() throws IOException{
        return socket.getInputStream();
    }
    
    public String getHostAddress(){
        return socket.getInetAddress().getHostAddress();
    }
    
/*--------------------Setters------------------*/
    
    public void setNombreUsuario(String nombre){
        nombreUsuario = nombre;
    }
    
    public void setSocket(Socket sock){
        socket = sock;
    }

   
}