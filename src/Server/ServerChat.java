package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;
import java.util.Vector;

public class ServerChat {

ServerSocket serverSocket;
BufferedReader bufferedReader;
PrintWriter printWritter;
static Socket socket;
static Vector<InfoCliente> clientes = new Vector<InfoCliente>();
String out1="";
int i=0;

/**
 * Manda un mensaje a todos los clientes conectados al servidor, exceptuando el cliente que mandó el mensaje.
 * 
 * @param mensaje Mensaje a enviar.
 * @param cliente Cliente que mandó el mensaje al servidor
 */
public void transmitir(String mensaje, InfoCliente cliente){
    
    for(InfoCliente cli : clientes){
        try{
            if(cli == cliente)
                continue;
            printWritter = new PrintWriter(cli.getSocket().getOutputStream(), true);
            printWritter.println(mensaje);
        
        }catch(NullPointerException | IOException e){
            continue;
        }
    }
    

}

/**
 * Elimina un cliente de la lista de clientes.
 * @param cli Cliente a eliminar.
 * @return Retorna true si el cliente fué eliminado, de lo contrario retorna false.
 */
public boolean eliminar(InfoCliente cli){
    return clientes.remove(cli);
}


/**
 * Inicia y conecta un servidor en un determinado puerto
 * @param port el puerto a utilizar.
 */
public ServerChat(int port) {
    try {
        
        //abre el socket para empezar a aceptar conexiones
        serverSocket = new ServerSocket(port);
        System.out.print("iniciado");

        while(true){
            
            //espera a una nueva conexion al servidor, se queda detenido en esta línea hasta que haya una conexión
            socket = serverSocket.accept();
            
            //en cuanto entra una nueva conexión, se crea un nuevo hilo que mantendrá la conexión con ese cliente para el chat
            Thread t1 = new Thread(){
                
               InfoCliente cliente;
               Socket auxSocket;
                public void run(){
                    
                    try{
                        
                        //para el inicio de la conexión, se deben arreglar algunas cosas:
                        
                        //se guarda el socket que se utilizará para el intercambio de mensajes entre el servidor y el cliente
                        auxSocket = socket;
                        
                        //Se guarda una instancia de BufferedReader que lee la entrada del socket
                        bufferedReader = new BufferedReader(new   InputStreamReader(auxSocket.getInputStream()));
                        
                        //el primer mensaje que se manda contiene solamente el nombre de usuario del cliente
                        String name = bufferedReader.readLine();
                        
                        //si no contiene nada, entonces se ignora la conexión y se cierra el socket
                        if(name == null){
                            auxSocket.close();
                            return;
                        }
                            
                            
                        //Se guarda una instancia de InfoCliente, que contiene la información relevante para la transmisión de mensajes.
                        cliente = new InfoCliente(name, auxSocket);
                        
                        //Se agrega el cliente al vector que guarda a todos los clientes conectados al servidor y lo muestra por consola
                        clientes.add(cliente);
                        System.out.println("Cliente conectado con ip " + cliente.getHostAddress() + " y nombre de usuario " + cliente.getNombreUsuario());
                        
                        //avisa a todos los demás clientes conectados al servidor que el nuevo cliente s ha conectado
                        transmitir(cliente.getNombreUsuario() + "Se ha conectado.\n", cliente);
                        
                        //loop para leer todos los mensajes que manda el cliente
                        do {
                            
                            try{
                                bufferedReader = new BufferedReader(new   InputStreamReader(cliente.getInputStream()));
                                
                                //Se lee el mensaje enviado por el cliente
                                out1 = bufferedReader.readLine();
                                
                                //si hay mensaje para enviar, entonces se transmite a los demás clientes
                                if(!(out1 == null))
                                    transmitir( cliente.getNombreUsuario() + " dice: " + out1, cliente);
                                else{
                                    // en caso de que el mensaje sea null, entonces se desconectó el cliente del servidor, por lo que se le debe eliminar
                                    String cli = cliente.getNombreUsuario();
                                    auxSocket.close();
                                    eliminar(cliente);
                                    transmitir(cli + " se ha desconectado :c", cliente);
                                    break;
                                    
                                }
                            }catch(SocketException e){
                                break;
                            }
                            
    
                        } while (true);
                    }catch(IOException e){
                    }
    
                }
            };t1.start();

        } 
    
    
    }catch (IOException e) {
        
    }    
 }

 

 


 public static void main(String[] args) {
     
     //inicia el servidor en el puerto 5000
    new ServerChat(5005);
}

 


}