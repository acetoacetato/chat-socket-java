package cliente;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import application.MainController;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


/**
 *
 * @author acetoacetato
 */
public class MainCliente extends Application {
    @Override
    public void start(Stage primaryStage) throws IOException {
        
        //inicia la ventana de chat
        primaryStage.getIcons().add(new Image("file:img/wsp.png"));
        FXMLLoader fxmlLoader =  new FXMLLoader( getClass().getResource( "/application/cosa.fxml"));
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root);
        MainController ventana = (MainController) fxmlLoader.getController();
        primaryStage.setScene(scene);
        
        //se le agrega una acción para terminar la aplicación de manera segura cuando se cierra la ventana
        primaryStage.setOnHiding(new EventHandler<WindowEvent>(){
            @Override
            public void handle(WindowEvent event){
                Platform.runLater(new Runnable(){
                
                    public void run(){
                        try {
                            ventana.cerrarConexion();
                            System.out.println("Se ha cerrado el chat.");
                            System.exit(0);
                            
                        } catch (IOException ex) {
                            System.out.println("Falló el intento de desconectar.");
                        }
                        
                    }
                
                });
                
                
            }
        
        });
        primaryStage.show();
        
        
        
                
       
    }

    /**
     * @param args the command line arguments
     **/
    public static void main(String[] args) {
        
        
        launch(args);
        
    }
    
}
