package application;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.application.Platform.exit;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
/**
 *
 * @author acetoacetato
 */
public class MainController implements Runnable, Initializable {
    
    
BufferedReader br;
PrintWriter pr;
Socket socket;

//hilo para recibir mensajes
Thread hiloRecibe;

String entrada = "", salida = "", direccion, puerto;


@FXML
private TextArea chat;
    
@FXML
private TextField campoTexto;
            
@FXML
private TextField nombreUsuario;
    
@FXML
private Label usrLabel;
    
@FXML
private Label serverStatus;

@FXML 
private Label dirLabel;

@FXML
private Label portLabel;

@FXML
private Button botonMandar;


@FXML
private TextField dir;

@FXML
private TextField port;


    /**
     * Realiza la conexión con el servidor 
     * @throws ConnectException En caso de no poder conectarse al servidor
     */
    public void conectar() throws IOException{

        //Se crea un hilo que tendrá la labor de recibir los mensajes que manda el servidor al cliente
        hiloRecibe = new Thread(this);

        //se crea un socket para conectarse al servidor
        socket = new Socket(direccion,Integer.parseInt(puerto));
        hiloRecibe.start();
        
    }
    
    /**
     * Agrega un mensaje a la ventana del chat
     * @param msj El mensaje a agregar
     */
    public void recibir(String msj){
        chat.appendText(msj);
    }
    
    
    /**
     * Envía un mensaje al servidor
     * @param evento El evento asociado a JavaFX
     * @throws IOException 
     */
    @FXML
    public void send(Event evento) throws IOException{
        
        //esconde el estado del servidor
        serverStatus.setVisible(false);
        //si se aprieta una tecla, pero dicha tecla no es 'enter'
        if(evento instanceof KeyEvent && ((KeyEvent) evento).getCode() != KeyCode.ENTER)
            return;
        
        
        //si el campo para ingresar el nombre de usuario se muestra, entonces es el primer mensaje a enviar al servidor
        if(nombreUsuario.isVisible()){
            String nombre = nombreUsuario.getText().trim();
            direccion = dir.getText().trim();
            puerto = port.getText().trim();
            //si el campo de texto está vacío
            if(nombre.equals("") | direccion.equals("") | puerto.equals(""))
                return;
            guardarPropiedades(direccion, puerto);
            try{
                //Se intenta conectar con el servidor dada la dirección y el puerto entregados por el usuario
               conectar(); 
            }catch(ConnectException e){
                //en caso de que no se pueda conectar, se da aviso al usuario
                serverStatus.setText("Servidor no disponible, verifique la dirección o que el servidor esté andando.");
                serverStatus.setVisible(true);
                return;
                
            }
            //si llega hasta aquí, ya se ha establecido conexión con el servidor, y se procede a identificar al usuario con este
            pr = new PrintWriter(socket.getOutputStream(), true);
            
            //Cambia la vista para mostrar el chat y el nombre del usuario
            pr.println(nombre);
            dir.setVisible(false);
            dirLabel.setVisible(false);
            port.setVisible(false);
            portLabel.setVisible(false);
            nombreUsuario.setVisible(false);
            campoTexto.setVisible(true);
            chat.appendText("Te has conectado al chat.\n");
            chat.setVisible(true);
            usrLabel.setText(nombre);
            usrLabel.setVisible(true);
            return;
            
            
        }
        
        
        pr = new PrintWriter(socket.getOutputStream(), true);
        
        //si se llega acá, ya está la conexión con el servidor y está listo para comenzar a mandar mensajes a los demás usuarios
        String msg = campoTexto.getText().trim();
        System.out.println(msg);
        if( msg.equals("") )
            return;
        chat.appendText("Tu: " + msg + "\n");
        
        //se le manda el mensaje al socket
        pr.println(msg);

        
        campoTexto.clear();
    }
    
    private Properties cargarPropiedades() throws IOException{
        
        Properties prop = new Properties();
        FileInputStream out = new FileInputStream("config.ini");
        prop.load(out);
        return prop;
    }
    
    private Properties guardarPropiedades(String dir, String puerto){
        FileOutputStream out = null;
    try {
        Properties prop = new Properties();
        out = new FileOutputStream("config.ini");
        prop.setProperty("Direccion", dir);
        prop.setProperty("Puerto", puerto);
        prop.store(out, null);
        return prop;
    } catch (IOException ex) {
        Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        System.exit(0);
    } 
    return null;
        
    }
    

    @FXML
    private void closeAction(ActionEvent event) throws IOException {
         Stage stage;
	 stage=(Stage) chat.getScene().getWindow();
         socket.close();
	 stage.close();
         exit();
}
    
    private void cerrarAplicacion() throws IOException, InterruptedException{
         
         socket.close();
         usrLabel.setVisible(false);
         chat.setVisible(false);
         serverStatus.setText("Se ha perdido la conexión con el servidor :c");
         serverStatus.setVisible(true);
         campoTexto.setVisible(false);
         botonMandar.setVisible(false);
         TimeUnit.SECONDS.sleep(3);
         
         
         System.exit(0);
    }
    
   
    
   
    /**
     * Hilo para recibir mensajes
     */
    @Override
    public void run(){
    
        try {

            do {
                //se abre el bufferedReader para leer el mensaje en el socket
                br = new BufferedReader(new   InputStreamReader(socket.getInputStream()));
                salida = br.readLine();
                if(salida == null){
                    cerrarAplicacion();
                }
                //se agrega el mensaje a la ventana de chat
                chat.appendText(salida + "\n");
                
            } while (true);
        
    } catch (IOException | NullPointerException | InterruptedException e) {
        System.out.println("no");
    }
    
    }
    
    /**
     * En caso de haberse establecido conexión con el servidor, se cierra la conexión con este
     * @throws IOException 
     */
    public void cerrarConexion() throws IOException{
        if(socket == null || hiloRecibe == null)
            return;
            
        socket.close();
        hiloRecibe.stop();
    }
    
 
    
    
    /**
     * Cambia la vista para colocar el último servidor al que se ha conectado el cliente
     * @param location
     * @param resources 
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    Properties propiedades;
    try {
        //intenta cargar las propiedades desde el archivo de estas
        propiedades = cargarPropiedades();    
    } catch (IOException ex) {
        //si no se pudieron cargar, entonces se guardan las propiedades por defecto: dirección localhost y puerto 5005
        propiedades = guardarPropiedades("localhost", "5005");
    }
    
    
    
    //Se obtienen la dirección y el puerto desde el archivo de propiedades
    direccion = propiedades.getProperty("Direccion");
    puerto = propiedades.getProperty("Puerto");
    
    //se cambian los campos de texto de dirección y puerto por los que habían en el archivo de propiedades
    dir.setText(direccion);
    port.setText(puerto);
    
    
    
    
        
    }

}
